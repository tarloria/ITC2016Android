﻿using UnityEngine;
using System.Collections;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Scene2_Global : MonoBehaviour
{
    private string filename = @"C:\Projects\Unity CSV Test 1\Assets\CSV\vr project csv scene2.csv";

    public List<double[]> data;

    public Slider slider;

    public Text Data_Raw;

    public GameObject[] obj_array;

    // Use this for initialization
    void Start()
    {
        print("Setting Data");
        data = new List<double[]>();
        setupData();
        slider.maxValue = data.Count - 1;
        print("Max: " + slider.maxValue.ToString());
        set_Data_Raw(0);

        set_obj_color(obj_array[0], (int)data[0][0]);
        set_obj_color(obj_array[1], (int)data[0][1]);
        set_obj_color(obj_array[2], (int)data[0][2]);
        set_obj_color(obj_array[3], (int)data[0][3]);
        set_obj_color(obj_array[4], (int)data[0][4]);
    }

    private void setupData()
    {
        data.Add(new double[] { 1, 1, 1, 1, 1 });
        data.Add(new double[] { 0, 1, 1, 1, 1 });
        data.Add(new double[] { 1, 1, 1, 0, 1 });
        data.Add(new double[] { 1, 0, 1, 0, 1 });
        data.Add(new double[] { 1, 0, 1, 0, 0 });
        data.Add(new double[] { 1, 1, 0, 0, 0 });
        data.Add(new double[] { 1, 1, 1, 0, 1 });
        data.Add(new double[] { 1, 1, 1, 0, 1 });
        data.Add(new double[] { 1, 1, 1, 0, 1 });
        data.Add(new double[] { 1, 1, 1, 0, 1 });
        data.Add(new double[] { 1, 1, 1, 1, 0 });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "Cube (0)")
                {
                    SceneManager.LoadScene("Scene0");
                }
                else if (hit.transform.name == "Cube (1)")
                {
                    SceneManager.LoadScene("Scene1");
                }
            }
        }
    }

    public void Back()
    {
        SceneManager.LoadScene("main");
    }

    private void set_obj_color(GameObject obj, int set)
    {
        if (set >= 1)
        {
            obj.GetComponent<Renderer>().material.color = Color.green;
        }
        else
        {
            obj.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    private void set_Data_Raw(int index_in)
    {
        Data_Raw.text =
            "Q1: " + data[index_in][0].ToString() + "\n" +
            "Q2: " + data[index_in][1].ToString() + "\n" +
            "Q3: " + data[index_in][2].ToString() + "\n" +
            "Q4: " + data[index_in][3].ToString() + "\n" +
            "Q5: " + data[index_in][4].ToString() + "\n";
    }

    public void SliderChange(float newIndex)
    {
        int index = (int)(newIndex);
        set_Data_Raw(index);

        set_obj_color(obj_array[0], (int)data[index][0]);
        set_obj_color(obj_array[1], (int)data[index][1]);
        set_obj_color(obj_array[2], (int)data[index][2]);
        set_obj_color(obj_array[3], (int)data[index][3]);
        set_obj_color(obj_array[4], (int)data[index][4]);
    }

}

